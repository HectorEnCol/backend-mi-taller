'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();


//-- Cargar rutas --
var usuario_rutas = require('./modulos/acceso/routes/usuario');
var empresa_rutas = require('./modulos/acceso/routes/empresa');
var sede_rutas = require('./modulos/acceso/routes/sede');

//-- Middlewares --
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//-- CORS --
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
 
    next();
});

//-- Rutas --
app.use('/api', usuario_rutas);
app.use('/api', empresa_rutas);
app.use('/api', sede_rutas);

//-- Exportar --
module.exports = app;