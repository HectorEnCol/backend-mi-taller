'use strict'

require('dotenv').config();

const { v4: uuidv4 } = require('uuid');

var fs = require('fs');
const sharp = require("sharp");

//-- Elimina un archivo del dispositivo de almacenamiento (el parámetro debe tener la ruta completa del archivo) --
function borrar(archivo) {
    fs.unlink(archivo, err => {
        if (!err) console.log(`Se ha eliminado el archivo: ${archivo}`);

        else console.log(`No se ha podido eliminar el archivo: ${archivo}`);
    });
}


function cambiarTamano(imagen) {

    var nombreTmp = `${process.env.RUTA_IMAGENES_USUARIO}${uuidv4()}.jpg`;

    sharp(imagen)
        .clone()
        .jpeg({ quality: 70 })
        .toFile(nombreTmp, (err, info) => {
            if (err) {
                console.error(err);
            } else {
                console.log(info);
                borrar(imagen);
                renombrar(nombreTmp, imagen);
            }
        });
}

function renombrar(nombreOriginal, nombreNuevo) {
    fs.rename(nombreOriginal, nombreNuevo, (err) => {
        if (err) throw err;
        console.log('Proceso Exitoso!!!');
    });
}

module.exports = {
    borrar,
    cambiarTamano,
}