'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'clave_generar_token_mi_taller_modulo_acceso_secreto';

exports.createToken = function(usuario){
    var payload = {
        sub: usuario._id,
        email: usuario.email,
        nombres: usuario.nombres,
        apellidos: usuario.apellidos,
        tipoId: usuario.tipoId,
        numeroId: usuario.numeroId,
        telefono: usuario.telefono,
        direccion: usuario.direccion,
        idPais: usuario.idPais,
        idDepartamento: usuario.idDepartamento,
        idMunicipio: usuario.idMunicipio,
        foto: usuario.foto,
        tipoUsuario: usuario.tipoUsuario,
        fechaCreacion: usuario.fechaCreacion,  
        activo: usuario.activo,
        iat: moment().unix(),
        exp: moment().add(30, 'days').unix()
    };

    return jwt.encode(payload, secret);
};