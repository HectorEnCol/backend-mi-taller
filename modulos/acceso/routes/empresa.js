'use strict'

var express = require('express');
var EmpresaController = require('../controllers/empresa');

var api = express.Router();
var md_aut = require('../middlewares/autenticador');

var crypto = require('crypto');
var multer = require('multer');
const storage = multer.diskStorage({
    destination(req, file, cb){
        cb(null, '../api/modulos/acceso/uploads/empresas');
    },
    filename(req, file = {}, cb){
        const { originalname } = file;

        const fileExtension = (originalname.match(/\.+[\S]+$/) || [])[0];
        crypto.pseudoRandomBytes(16, function(err, raw){
            cb(null, raw.toString('hex') + Date.now() + fileExtension);
        });
    },
});

var mul_upload = multer({dest: '../api/modulos/acceso/uploads/empresas', storage});

api.get('/pruebas-empresa', md_aut.asegurarAutenticacion, EmpresaController.pruebas);
api.post('/registro-empresa/:idUsuario', md_aut.asegurarAutenticacion, EmpresaController.crearEmpresa);
api.put('/modificar-empresa/:idUsuario/:id', md_aut.asegurarAutenticacion, EmpresaController.modificarDatosEmpresa);
api.post('/modificar-imagen-empresa/:idUsuario/:id', [md_aut.asegurarAutenticacion, mul_upload.single('logo')], EmpresaController.subirImagenEmpresa);
api.get('/empresa/:idUsuario/:id', md_aut.asegurarAutenticacion, EmpresaController.obtenerDatosEmpresa);
api.get('/lista-empresas/:idUsuario?/:pagina?', md_aut.asegurarAutenticacion, EmpresaController.obtenerEmpresasPorUsuario);
api.get('/obtener-imagen-empresa/:idUsuario/:archivoImagen', md_aut.asegurarAutenticacion, EmpresaController.obtenerImagen);

module.exports = api;