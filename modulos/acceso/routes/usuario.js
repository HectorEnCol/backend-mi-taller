'use strict'

require('dotenv').config();

var express = require('express');
var UsuarioController = require('../controllers/usuario');

var api = express.Router();
var md_aut = require('../middlewares/autenticador');

var crypto = require('crypto');
var multer = require('multer');
const storage = multer.diskStorage({
    destination(req, file, cb){
        cb(null, process.env.RUTA_IMAGENES_USUARIO);
    },
    filename(req, file = {}, cb){
        const { originalname } = file;

        const fileExtension = (originalname.match(/\.+[\S]+$/) || [])[0];
        crypto.pseudoRandomBytes(16, function(err, raw){
            cb(null, raw.toString('hex') + Date.now() + fileExtension);
        });
    },
});

var mul_upload = multer({ dest: process.env.RUTA_IMAGENES_USUARIO, storage });

api.get('/', UsuarioController.home);
api.get('/pruebas', md_aut.asegurarAutenticacion, UsuarioController.pruebas);
api.post('/registro', UsuarioController.crearUsuario);
api.post('/login', UsuarioController.loginUsuario);
api.put('/modificar-contrasena/:id', md_aut.asegurarAutenticacion, UsuarioController.modificarContrasena);
api.put('/modificar-usuario/:id', md_aut.asegurarAutenticacion, UsuarioController.modificarDatosUsuario);
api.post('/modificar-imagen-usuario/:id', [md_aut.asegurarAutenticacion, mul_upload.single('foto')], UsuarioController.subirImagenUsuario);
api.get('/usuario/:id', md_aut.asegurarAutenticacion, UsuarioController.obtenerDatosUsuario);
api.get('/obtener-imagen-usuario/:archivoImagen', UsuarioController.obtenerImagen);
api.get('/usuarios-lista/:pagina?', md_aut.asegurarAutenticacion, UsuarioController.obtenerUsuarios);

module.exports = api;