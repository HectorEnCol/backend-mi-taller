'use strict'

var express = require('express');
var SedeController = require('../controllers/sede');

var api = express.Router();
var md_aut = require('../middlewares/autenticador');

api.get('/pruebas-sede', md_aut.asegurarAutenticacion, SedeController.pruebas);
api.post('/registro-sede/:idEmpresa', md_aut.asegurarAutenticacion, SedeController.crearSede);
api.put('/modificar-sede/:id', md_aut.asegurarAutenticacion, SedeController.modificarDatosSede);
api.get('/sede/:idSede', md_aut.asegurarAutenticacion, SedeController.obtenerDatosSede);
api.get('/lista-sedes/:idEmpresa/:pagina', md_aut.asegurarAutenticacion, SedeController.obtenerSedesPorEmpresa);



module.exports = api;