'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RelSedeSisMunicipioSchema = Schema({
    sede: { type: Schema.ObjectId, ref: 'Sede' },
    municipio: { type: Schema.ObjectId, ref: 'SisMunicipio' }   
});

module.exports = mongoose.model('RelSedeSisMunicipio', RelSedeSisMunicipioSchema);