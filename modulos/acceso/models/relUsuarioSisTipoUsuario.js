'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RelUsuarioSisTipoUsuarioSchema = Schema({
    usuario: { type: Schema.ObjectId, ref: 'Usuario' },
    tipoUsuario: { type: Schema.ObjectId, ref: 'SisTipoUsuario' }   
});

module.exports = mongoose.model('RelUsuarioSisTipoUsuario', RelUsuarioSisTipoUsuarioSchema);