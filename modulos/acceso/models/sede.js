'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SedeSchema = Schema({
    idEmpresa: String,
    nombre: String,
    direccion: String,
    idPais: Number,
    idDepartamento: Number,
    idMunicipio: Number, 
    email: String,
    telefono: String,
    telefonoAuxiliar: String,
    fechaCreacion: String,
    activo: Boolean
});

module.exports = mongoose.model('Sede', SedeSchema);