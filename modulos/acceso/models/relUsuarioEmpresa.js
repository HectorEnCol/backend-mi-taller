'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RelUsuarioEmpresaSchema = Schema({
    usuario: { type: Schema.ObjectId, ref: 'Usuario' },
    empresa: { type: Schema.ObjectId, ref: 'Empresa' }   
});

module.exports = mongoose.model('RelUsuarioEmpresa', RelUsuarioEmpresaSchema);