'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RelUsuarioSedeSchema = Schema({
    usuario: { type: Schema.ObjectId, ref: 'Usuario' },
    sede: { type: Schema.ObjectId, ref: 'Sede' }   
});

module.exports = mongoose.model('RelUsuarioSede', RelUsuarioSedeSchema);