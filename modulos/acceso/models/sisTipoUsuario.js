'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SisTipoUsuarioSchema = Schema({
    idTipo: Number,
    tipoUsuario: String
});

module.exports = mongoose.model('SisTipoUsuario', SisTipoUsuarioSchema);