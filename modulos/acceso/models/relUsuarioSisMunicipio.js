'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RelUsuarioSisMunicipioSchema = Schema({
    usuario: { type: Schema.ObjectId, ref: 'Usuario' },
    municipio: { type: Schema.ObjectId, ref: 'SisMunicipio' }   
});

module.exports = mongoose.model('RelUsuarioSisMunicipio', RelUsuarioSisMunicipioSchema);