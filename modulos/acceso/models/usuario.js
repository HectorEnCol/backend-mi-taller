'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UsuarioSchema = Schema({
    email: String,
    contrasena: String,
    nombres: String,
    apellidos: String,
    tipoId: String,
    numeroId: Number,
    telefono: String,
    direccion: String,
    idPais: Number,
    idDepartamento: Number,
    idMunicipio: Number,
    foto: String,
    tipoUsuario: String,
    fechaCreacion: String,
    activo: Boolean
});

module.exports = mongoose.model('Usuario', UsuarioSchema);