'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EmpresaSchema = Schema({
    idUsuario: String,
    tipoEmpresa: String,
    idEmpresa: String,
    nombre: String,
    logo: String,
    fechaCreacion: String,
    activo: Boolean
});

module.exports = mongoose.model('Empresa', EmpresaSchema);