'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SisMunicipioSchema = Schema({
    idPais: Number,
    nombrePais: String,
    idDepartamento: Number,
    nombreDepartamento: String,
    idMunicipio: Number,
    nombreMunicipio: String
});

module.exports = mongoose.model('SisMunicipio', SisMunicipioSchema);