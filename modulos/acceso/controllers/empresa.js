'use strict'

var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var fs = require('fs');
var path = require('path');
var mongoosePaginate = require('mongoose-pagination');

var Empresa = require('../models/empresa');
var Usuario = require('../models/usuario');
var RelUsuarioEmpresa = require('../models/relUsuarioEmpresa');

var imagen = require('../services/imagen');


const usuario = require('../models/usuario');
const relUsuarioEmpresa = require('../models/relUsuarioEmpresa');

//-- Función para probar la ruta del servidor --
function pruebas(req, res){
    console.log(req.body);
    res.status(200).send({message: 'Prueba del servidor NodeJS desde el controlador empresa'});
}

//-- Crea una nueva empresa --
function crearEmpresa(req, res){
    var params = req.body;
    var idUsuario = req.params.idUsuario;
    var empresa = new Empresa();
    
    if(idUsuario != req.usuario.sub){
        return res.status(500).send({message: 'No tienes permiso para realizar la operación'});
    }

    //-- Valida que los campos obligatorios lleguen como parámetro --
    if(params.tipoEmpresa && params.idEmpresa && params.nombre){
        empresa.idUsuario = idUsuario;
        empresa.tipoEmpresa = params.tipoEmpresa;
        empresa.idEmpresa = params.idEmpresa;
        empresa.nombre = params.nombre;
        empresa.logo = null; 
        empresa.fechaCreacion = moment().unix();
        empresa.activo = true;

        //-- Validación de empresa existente en la Base de Datos --
        Empresa.find({ $and: [
            { idUsuario: empresa.idUsuario },
            { tipoEmpresa: empresa.tipoEmpresa },
            { idEmpresa: empresa.idEmpresa } 
        ]}).exec((err, empresas) =>{
            if(err) return res.status(500).send({message: 'Error en la petición de empresas'});

            if(empresas && empresas.length >= 1){
                return res.status(200).send({message: 'La empresa que intentas registrar ya existe en el sistema!!!'});
            }else{

        
                //-- Almacena la empresa en la Base de Datos --
                empresa.save((err, empresaAlmacenada) => {
                    if(err) return res.status(500).send({message: 'Error al guardar la empresa'});

                    if(empresaAlmacenada){
                        res.status(200).send({empresa: empresaAlmacenada});
                    }else{
                        res.status(404).send({message: 'No se ha registrado la empresa'});
                    }
                });

                crearRelUsuarioEmpresa(req.usuario.sub, empresa, res);//
            }
        });

    //-- Opción, los campos obligatorios no llegan completos --
    }else{
        res.status(200).send({message: 'Los campos solicitados son obligatorios...'});
    }
}

//-- Función complementaria para crear una relación entre usuario y empresa (usuario que pertenece a una empresa) --
function crearRelUsuarioEmpresa(idUsuario, empresa, res){
    var usuario = new Usuario();
    var relUsuarioEmpresa = new RelUsuarioEmpresa();
    usuario.idUsuario = idUsuario;

    Usuario.findOne({_id: usuario.idUsuario}).exec((err, usuario) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!usuario) return res.status(404).send({message: 'Usuario no encontrado'});

        relUsuarioEmpresa.empresa = empresa._id;
        relUsuarioEmpresa.usuario = usuario._id;

        relUsuarioEmpresa.save();
    });
}

//-- Adicionar o Modificar datos de una empresa -- Permite habilitar o inhabilitar empresa --
function modificarDatosEmpresa(req, res){
    var empresaId = req.params.id;
    var usuarioId = req.params.idUsuario;
    var update = req.body;

    if(usuarioId != req.usuario.sub){
        return res.status(500).send({message: 'No tienes permiso para actualizar los datos de la empresa...'});
    }

    Empresa.findByIdAndUpdate(empresaId, update, {new:true}, (err, empresaModificada) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!empresaModificada) return res.status(404).send({message: 'No se ha podido registrar la información...'});

        return res.status(200).send({empresa: empresaModificada});
    });
}

//-- Regresa todos los datos de una empresa registrada en el sistema --
function obtenerDatosEmpresa(req, res){
    var usuarioId = req.params.idUsuario;
    var empresaId = req.params.id;

    if(usuarioId != req.usuario.sub){
        return res.status(500).send({message: 'No tienes permiso para obtener información'});
    }

    Empresa.findById(empresaId, (err, empresa) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!empresa) return res.status(404).send({message: 'La empresa no existe'});

        return res.status(200).send({
            empresa
        });
    });
}

//-- Función que regresa el listado de empresas relacionadas con el usuario logueado al sistema --
function obtenerEmpresasPorUsuario(req, res){
    var usuarioId = req.usuario.sub;

    if(req.params.idUsuario && req.params.pagina){
        usuarioId = req.params.idUsuario;
    }

    var pagina = 1;

    if(req.params.pagina){
        pagina = req.params.pagina;
    }else{
        pagina = req.params.idUsuario;
    }

    var itemsPorPagina = 10;

    RelUsuarioEmpresa.find({usuario: usuarioId}).populate({path: 'empresa'}).paginate(pagina, itemsPorPagina, (err, empresas, total) => {
        if(err) return res.status(500).send({message: 'Error en el servidor'});

        if(!empresas) return res.status(404).send({message: 'No has registrado ninguna empresa'});

        return res.status(200).send({
            total: total,
            paginas: Math.ceil(total/itemsPorPagina),
            empresas,
        });
    });
}

//-- Función que obtiene una imagen desde la Base de Datos por id de imagen --
function obtenerImagen(req, res){
    var usuarioId = req.params.idUsuario;
    var image_file = req.params.archivoImagen;
    var path_file = '../api/modulos/acceso/uploads/empresas/' + image_file;

    if(usuarioId != req.usuario.sub){
        return res.status(500).send({message: 'No tienes permiso para obtener información'});
    }

    fs.exists(path_file, (existe) => {
        if(existe){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message: 'No existe la imagen...'});
        }
    });
}

//-- Subir archivos de imagen/logo de empresa --
function subirImagenEmpresa(req, res){
    var empresaId = req.params.id;
    var usuarioId = req.params.idUsuario;
    var imagenAntigua = '';

    //-- Busca el usuario al que se le reemplazará la imagen --
    Empresa.findById(empresaId, (err, empresa) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(empresa && req.file){
            imagenAntigua = empresa.logo;
            file_path_aux += imagenAntigua;
            
            imagen.borrar(file_path_aux);
        }else{
            return res.status(404).send({message: 'No se han hallado los datos requeridos'});
        }
    });

    if(req.file){
        //-- Captura las propiedades del archivo --
        var file_path = req.file.path;
        var file_split = file_path.split('\\');
        var file_path_aux = '';

        for(var i = 0; i < 6; i++){
            file_path_aux += file_split[i] + '\\';
        }

        var file_name = file_split[6];
        var ext_split = req.file.originalname.split('\.');
        var file_ext = ext_split[1];

        if(usuarioId != req.usuario.sub){
            imagen.borrar(file_path);
            return res.status(500).send({message: 'No tienes permiso para actualizar los datos del usuario'});        
        }

        //-- Analiza que la extensión de la imagen sea de formato válido --
        if(file_ext == 'png' || file_ext == 'gif' || file_ext == 'jpg' || file_ext == 'jpeg'){
            Empresa.findByIdAndUpdate(empresaId, {logo: file_name}, {new: true}, (err, empresaModificada) => {
                if(err){
                    imagen.borrar(file_path);
                    return res.status(500).send({message: 'Error en la petición'});
                } 

                if(!empresaModificada){
                    imagen.borrar(file_path);
                    return res.status(404).send({message: 'No se ha podido actualizar la información'});                    
                }
                
                return res.status(200).send({empresa: empresaModificada});
            })
        }else{
            //-- Elimina el archivo del almacenamiento si la extensión no es válida --
            imagen.borrar(file_path);
            return res.status(500).send({message: 'Extensión no válida'});         }
    }else{
        res.status(200).send({message: 'No has subido nunguna imagen'});
    }
}


module.exports = {
    pruebas,
    crearEmpresa,
    modificarDatosEmpresa,
    obtenerDatosEmpresa,
    obtenerEmpresasPorUsuario,
    obtenerImagen,
    subirImagenEmpresa
}