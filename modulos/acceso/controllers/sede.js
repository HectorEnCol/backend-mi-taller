'use strict'

var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var fs = require('fs');
var path = require('path');
var mongoosePaginate = require('mongoose-pagination');

var Sede = require('../models/sede');

//-- Función para probar la ruta del servidor --
function pruebas(req, res){
    console.log(req.body);
    res.status(200).send({message: 'Prueba del servidor NodeJS desde el controlador sede'});
}

//-- Crea una nueva sede --
function crearSede(req, res){
    var params = req.body;
    var idEmpresa = req.params.idEmpresa;
    var sede = new Sede();

    //-- Valida que los campos obligatorios lleguen como parámetro --
    if(params.nombre){
        sede.idEmpresa = idEmpresa;
        sede.nombre = params.nombre;
        sede.direccion = params.direccion;
        sede.idPais = params.idPais;
        sede.idDepartamento = params.idDepartamento;
        sede.idMunicipio = params.idMunicipio; 
        sede.email = params.email;
        sede.telefono = params.telefono;
        sede.telefonoAuxiliar = params.telefonoAuxiliar;
        sede.fechaCreacion = moment().unix();    
        sede.activo = true;    

        //-- Validación de empresa existente en la Base de Datos --
        Sede.find({ $and: [
            { idEmpresa: sede.idEmpresa },
            { nombre: sede.nombre }
        ]}).exec((err, sedes) =>{
            if(err) return res.status(500).send({message: 'Error en la petición de sedes'});

            if(sedes && sedes.length >= 1){
                return res.status(200).send({message: 'La sede que intentas registrar ya existe en el sistema!!!'});
            }else{

                //-- Almacena la sede en la Base de Datos --
                sede.save((err, sedeAlmacenada) => {
                    if(err) return res.status(500).send({message: 'Error al guardar la sede'});

                    if(sedeAlmacenada){
                        res.status(200).send({sede: sedeAlmacenada});
                    }else{
                        res.status(404).send({message: 'No se ha registrado la sede'});
                    }
                });
            }
        });

    //-- Opción, los campos obligatorios no llegan completos --
    }else{
        res.status(200).send({message: 'Los campos solicitados son obligatorios...'});
    }
}

//-- Adicionar o Modificar datos de una sede -- Permite habilitar o inhabilitar sede --
function modificarDatosSede(req, res){
    var sedeId = req.params.id;
    var update = req.body;

    Sede.findByIdAndUpdate(sedeId, update, {new:true}, (err, sedeModificada) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!sedeModificada) return res.status(404).send({message: 'No se ha podido registrar la información...'});

        return res.status(200).send({sede: sedeModificada});
    });
}

//-- Regresa todos los datos de una empresa registrada en el sistema --
function obtenerDatosSede(req, res){
    //var empresaId = req.params.idEmpresa;
    var sedeId = req.params.idSede;

    Sede.findById(sedeId, (err, sede) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!sede) return res.status(404).send({message: 'La sede no existe'});

        return res.status(200).send({
            sede
        });
    });
}

//-- Función que regresa el listado de sedes relacionadas con una empresa (por id almacenado) --
function obtenerSedesPorEmpresa(req, res){
    //var usuarioId = req.usuario.sub;
    var empresaId = '';

    if(req.params.idEmpresa && req.params.pagina){
        empresaId = req.params.idEmpresa;
    }

    var pagina = 1;

    if(req.params.pagina){
        pagina = req.params.pagina;
    }else{
        pagina = req.params.idUsuario;
    }

    var itemsPorPagina = 10;

    Sede.find({idEmpresa: empresaId}).populate('sede').paginate(pagina, itemsPorPagina, (err, sedes, total) => {
        if(err) return res.status(500).send({message: 'Error en el servidor'});

        if(!sedes) return res.status(404).send({message: 'No hay sedes asociadas a la empresa'});

        return res.status(200).send({
            total: total,
            paginas: Math.ceil(total/itemsPorPagina),
            sedes,
        });
    });
}

module.exports = {
    pruebas,
    crearSede,
    modificarDatosSede,
    obtenerDatosSede,
    obtenerSedesPorEmpresa
}