'use strict'
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var fs = require('fs');
var path = require('path');
var mongoosePaginate = require('mongoose-pagination');

var Usuario = require('../models/usuario');
var jwt = require('../services/jwt');
var imagen = require('../services/imagen');
var parametros = require('../services/parametros');

const { param } = require('../routes/usuario');
const { log } = require('console');

//-- Función Hola Mundo, proceso de prueba del servidor --
function home(req, res){
    res.status(200).send({message: 'Hola Mundo desde el servidor NodeJS'});
}

//-- Función para probar la ruta del servidor --
function pruebas(req, res){
    console.log(req.body);
    res.status(200).send({message: 'Prueba método del servidor NodeJS'});
}

//-- Crea un nuevo usuario ingresando la información en el sistema (e-mail y contraseña) --
function crearUsuario(req, res){
    var params = req.body;
    var usuario = new Usuario();

    //-- Valida que los campos obligatorios lleguen como parámetro --
    if(params.email && params.contrasena && params.confirmaContrasena){
        usuario.email = params.email;
        usuario.nombres = null;
        usuario.apellidos = null;
        usuario.tipoId = null;
        usuario.numeroId = 0;
        usuario.telefono = null;
        usuario.direccion = null;
        usuario.idPais = 0;
        usuario.idDepartamento = 0;
        usuario.idMunicipio = 0;
        usuario.foto = null;
        usuario.tipoUsuario = 'ADMINISTRADOR DEL SISTEMA'; 
        usuario.fechaCreacion = moment().unix();
        usuario.activo = true;

        //-- Validación de usuario existente en la Base de Datos --
        Usuario.find({
            email: usuario.email.toLowerCase()
        }).exec((err, usuarios) =>{
            if(err) return res.status(500).send({message: 'Error en la petición de usuarios'});

            if(usuarios && usuarios.length >= 1){
                return res.status(200).send({message: 'El usuario que intentas registrar ya existe!!!'});
            }else{
                //-- Compara las contraseñas parametrizadas --
                if(params.contrasena == params.confirmaContrasena){
                    bcrypt.hash(params.contrasena, null, null, (err, hash) => {
                        usuario.contrasena = hash;
        
                        //-- Almacena el usuario en la Base de Datos --
                        usuario.save((err, usuarioAlmacenado) => {
                            if(err) return res.status(500).send({message: 'Error al guardar el usuario'});
        
                            if(usuarioAlmacenado){
                                res.status(200).send({usuario: usuarioAlmacenado});
                            }else{
                                res.status(404).send({message: 'No se ha registrado el usuario'});
                            }
                        });
                    });
                }else{
                    res.status(200).send({message: 'Las contraseñas no coinciden...'});
                }
            }
        });

    //-- Opción, los campos obligatorios no llegan completos --
    }else{
        res.status(200).send({message: 'Los campos solicitados son obligatorios...'});
    }
}

//-- Accede con un usuario al sistema (logueo) --
function loginUsuario(req, res){
    var params = req.body;
    var email = params.email;
    var contrasena = params.contrasena;

    //-- Busca al usuario en la Base de Datos --
    Usuario.findOne({email: email}, (err, usuario) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(usuario){
            //-- Compara la contraseña del parámetro con la almacenada en la Base de Datos --
            bcrypt.compare(contrasena, usuario.contrasena, (err, check) => {
                if(err) return res.status(500).send({message: 'Error en la petición'});

                if(check){
                    if(params.gettoken){
                        //-- Generar y devolver token --
                        return res.status(200).send({
                            token: jwt.createToken(usuario)
                        });
                    }else{
                        //-- Anula el parámetro contraseña para no retornarlo con la información de la Base de Datos --
                        usuario.contrasena = undefined;
                        //-- Devuelve los datos del usuario --
                        return res.status(200).send({usuario});
                    }

                }else{
                    return res.status(404).send({message: 'El usuario no se ha podido identificar!'});
                }
            });
        }else{
            return res.status(404).send({message: 'El usuario no se ha podido identificar!!!'});
        }
    });
}

//-- Modificar contraseña --
function modificarContrasena(req, res){
    var usuarioId = req.params.id;
    var params = req.body;

    if(usuarioId != req.usuario.sub){
        return res.status(500).send({message: 'No tienes permiso para actualizar los datos del usuario'});
    }

    //-- Busca al usuario en la Base de Datos --
    Usuario.findById(usuarioId, (err, usuario) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(usuario){
            //-- Compara la contraseña del parámetro con la almacenada en la Base de Datos --
            bcrypt.compare(params.contrasenaActual, usuario.contrasena, (err, check) => {
                if(err) return res.status(500).send({message: 'Error en la petición'});

                //-- Opción si la contraseña ingresada no coincide con la almacenada --
                if(!check){
                    return res.status(404).send({message: 'No se reconoce la contraseña actual!!!'});
                }else{
                    //-- Compara la contraseña nueva con la existente en el sistema --
                    bcrypt.compare(params.contrasena, usuario.contrasena, (err, check) => {
                        if(err) return res.status(500).send({message: 'Error en la petición'});

                        //-- Opción si la contraseña nueva es igual a la almacenada en la BD --
                        if(check){
                            return res.status(404).send({message: 'La contraseña nueva no puede ser igual a la anterior'});
                        }else{
                            bcrypt.hash(params.contrasena, null, null, (err, hash) => {
                                if(err) return res.status(500).send({message: 'Error en la petición'});

                                params.contrasena = hash;

                                Usuario.findByIdAndUpdate(usuarioId, params, {new:true}, (err, usuarioModificado) => {
                                    if(err) return res.status(500).send({message: 'Error en la petición'});
                            
                                    if(!usuarioModificado) return res.status(404).send({message: 'No se ha podido modificar la contraseña...'});
                            
                                    return res.status(200).send({usuario: usuarioModificado});
                                });
                            });
                        }
                    });
                }
            });
        }else{
            return res.status(404).send({message: 'El usuario no se ha podido identificar!!!'});
        }
    });
}

//-- Adicionar o Modificar datos de un usuario -- Permite habilitar o inhabilitar usuario --
function modificarDatosUsuario(req, res){
    var usuarioId = req.params.id;
    var update = req.body;

    //-- Borrar propiedad contraseña --
    delete update.contrasena;

     if(usuarioId != req.usuario.sub){
        return res.status(500).send({message: 'No tienes permiso para actualizar los datos del usuario'});
    }

    Usuario.findByIdAndUpdate(usuarioId, update, {new:true}, (err, usuarioModificado) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!usuarioModificado) return res.status(404).send({message: 'No se ha podido registrar la información...'});

        return res.status(200).send({usuario: usuarioModificado});
    });
}

//-- Regresa todos los datos de un usuario registrado en el sistema --
function obtenerDatosUsuario(req, res){
    var usuarioId = req.params.id;

    Usuario.findById(usuarioId, (err, usuario) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!usuario) return res.status(404).send({message: 'El usuario no existe'});

        return res.status(200).send({
            usuario
        });
    });
}

//-- Función que obtiene una imagen desde la Base de Datos por id de imagen --
function obtenerImagen(req, res){
    var image_file = req.params.archivoImagen;
    var path_file = '../api/modulos/acceso/uploads/usuarios/' + image_file;

    fs.exists(path_file, (existe) => {
        if(existe){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message: 'No existe la imagen...'});
        }
    });
}

//-- Lista todos los usuarios del sistema por páginas --
function obtenerUsuarios(req, res){
    var usuarioIdRegistrado = req.usuario.sub;

    var pagina = 1;
    if(req.params.pagina){
        pagina = req.params.pagina;
    }

    //var itemsPorPagina = parametros.ITEMS_POR_PAGINA;
    var itemsPorPagina = 10;

    Usuario.find().sort('_id').paginate(pagina, itemsPorPagina, (err, usuarios, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!usuarios) return res.status(404).send({message: 'No hay usuarios disponibles'});

        return res.status(200).send({
            usuarios,
            total,
            paginas: Math.ceil(total/itemsPorPagina)
        });
    });
}

//-- Subir archivos de imagen/avatar de usuario --
function subirImagenUsuario(req, res){
    var usuarioId = req.params.id;
    var imagenAntigua = '';

    //-- Busca el usuario al que se le reemplazará la imagen --
    Usuario.findById(usuarioId, (err, usuario) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(usuario && req.file){
            imagenAntigua = usuario.foto;
            file_path_aux += imagenAntigua;

            imagen.borrar(file_path_aux);
        }else{
            return res.status(404).send({message: 'No se han hallado los datos requeridos'});
        }
    });

    if(req.file){
        //-- Captura las propiedades del archivo --
        var file_path = req.file.path;    
        var file_split = file_path.split('\\');
        var file_path_aux = '';

        for(var i = 0; i < 6; i++){
            file_path_aux += file_split[i] + '\\';
        }

        var file_name = file_split[6];
        var ext_split = req.file.originalname.split('\.');
        var file_ext = ext_split[1];

        if(usuarioId != req.usuario.sub){
            imagen.borrar(file_path);
            return res.status(500).send({message: 'No tienes permiso para actualizar los datos del usuario'});
        }

        //-- Analiza que la extensión de la imagen sea de formato válido --
        if(["png", "gif", "jpg", "jpeg"].includes(file_ext)){
            
            imagen.cambiarTamano(file_path);

            Usuario.findByIdAndUpdate(usuarioId, {foto: file_name}, {new: true}, (err, usuarioModificado) => {
                if(err){
                    imagen.borrar(file_path);
                    return res.status(500).send({message: 'Error en la petición'});
                } 

                if(!usuarioModificado){
                    imagen.borrar(file_path);
                    return res.status(404).send({message: 'No se ha podido actualizar la información'});                    
                }

                return res.status(200).send({usuario: usuarioModificado});
            });
        }else{
            //-- Elimina el archivo del almacenamiento si la extensión no es válida --
            imagen.borrar(file_path);
            return res.status(500).send({message: 'Extensión no válida'});            
        }
    }else{
        res.status(200).send({message: 'No has subido nunguna imagen'});
    }
}



module.exports = {
    home,
    pruebas,
    crearUsuario,
    loginUsuario,
    modificarContrasena,
    modificarDatosUsuario,
    obtenerDatosUsuario,
    obtenerImagen,
    obtenerUsuarios,
    subirImagenUsuario
}